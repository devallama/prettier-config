module.exports = {
    endOfLine: 'auto',
    singleQuote: true,
    tabWidth: 2,
    trailingComma: 'none',
    arrowParens: 'avoid',
    jsxBracketSameLine: false,
    bracketSpacing: true,
    jsxSingleQuote: false,
    quoteProps: 'preserve',
    printWidth: 100,
};
